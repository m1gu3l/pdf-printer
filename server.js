const express = require("express");
const uuid = require("uuid");
const fs = require("fs");
const path = require("path");

module.exports = async function start({ browser = null, port = 80 } = {}) {
    if (!browser) {
        throw new Error(`no browser`);
    }

    try {
        const page = await browser.newPage();
        page.close();
    } catch {
        throw new Error(`browser not working`);
    }

    const pdfFolder = path.resolve(__dirname, "./pdf");
    fs.existsSync(pdfFolder) || fs.mkdirSync(pdfFolder);

    const htmlFolder = path.resolve(__dirname, "./html");
    fs.existsSync(htmlFolder) || fs.mkdirSync(htmlFolder);

    const app = express();
    app.use(require("body-parser").text());

    app.get("/print", (async (req, res) => {
        const url = req.query.url;

        if (!url) {
            res.status(400);
            res.json({ message: `url query parameter required` });
            return;
        }

        try {
            new URL(url);
        } catch {
            res.status(400);
            res.json({ message: `invalid url query parameter` });
        }

        try {
            const id = uuid.v1();

            const page = await browser.newPage();
            await page.goto(url, { waitUntil: "networkidle2" });
            await page.pdf({ path: path.resolve(pdfFolder, `${id}.pdf`), format: "A4" });
            await page.close();

            res.redirect(`/pdf/${id}.pdf`);
        } catch (error) {
            res.status(500);
            res.json({ message: error.message, stack: error.stack });
        }
    }));

    app.get("/ping", (req, res) => {
        res.status(200);
        res.send("pong");
    });

    app.post("/print", async (req, res) => {
        try {
            const id = uuid.v1();
            const file = path.resolve(htmlFolder, `${id}.html`);
            fs.writeFileSync(file, req.body, "utf8");

            const page = await browser.newPage();
            await page.goto(`http://localhost:${port}/html/${id}.html`, { waitUntil: "networkidle2" });
            await page.pdf({ path: path.resolve(pdfFolder, `${id}.pdf`), format: "A4" });
            await page.close();

            res.redirect(`/pdf/${id}.pdf`);
        } catch (error) {
            res.status(500);
            res.json({ message: error.message, stack: error.stack });
        }
    });

    app.get("/html/:file", (req, res) => {
        const file = path.resolve(htmlFolder, req.params.file);
        if (fs.existsSync(file)) {
            res.status(200);
            res.sendFile(file);
            res.once("finish", () => fs.unlinkSync(file));
        } else {
            res.status(404);
            res.json({ message: `no file found` });
        }
    });

    app.get("/pdf/:file", (req, res) => {
        const file = path.resolve(pdfFolder, req.params.file);
        if (fs.existsSync(file)) {
            res.status(200);
            res.download(file);
            res.once("finish", () => fs.unlinkSync(file));
        } else {
            res.status(404);
            res.json({ message: `no file found` });
        }
    });

    console.log(`starting server on port ${port}`);

    return app.listen(port);
};

