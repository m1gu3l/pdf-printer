PDF-PRINTER
----

GET /ping
---> "pong"

GET /print?url=http://google.com
---> pdf

POST /print "<html><body></body></html>"
---> pdf