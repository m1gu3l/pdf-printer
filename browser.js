const p = require("puppeteer");

const isLinux = process.platform === "linux";
const LINUX_CHROMIUM = "/usr/bin/chromium-browser";
const WINDOWS_CHROME = `C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe`;

module.exports = async ({ port, host }) => {
    const options = isLinux
        ? {
            headless: true,
            executablePath: LINUX_CHROMIUM,
            args: [
                "--no-sandbox",
                "--disable-gpu",
                "--window-size=1200,1200",
                `--remote-debugging-port=${port}`,
                `--remote-debugging-address=${host}`,
            ],
            userDataDir: "/usr/cache",
        }
        : {
            headless: true,
            executablePath: WINDOWS_CHROME,
            args: [
                "--window-size=1200,1200",
                `--remote-debugging-port=${port}`,
                `--remote-debugging-address=${host}`,
            ],
        };

    return await p.launch(options);
}

