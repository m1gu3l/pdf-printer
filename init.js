const yargs = require("yargs");

const createBrowser = require("./browser");
const createServer = require("./server");

(async function init() {
    const { chromeHost, chromePort, serverPort } = yargs
        .string("chromeHost")
        .number("chromePort")
        .number("serverPort")
        .default("chromeHost", "127.0.0.1")
        .default("chromePort", 9222)
        .default("serverPort", 80)
        .argv;

    const browser = await createBrowser({ host: chromeHost, port: chromePort });
    const server = await createServer({ browser, port: serverPort });

    process.on("beforeExit", () => {
        browser.close();
        server.close();
    })

})();